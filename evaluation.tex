% -*- root: crawler-paper.tex -*-
\section{Evaluation}
\labsection{evaluation}

In this section, we evaluate the performance of \crawler along several key metrics such as the page processing rate, the memory usage and the network traffic across sites.
We split this evaluation in two parts.
First, we evaluate our approach in-vitro, by running \crawler against the ClueWeb12 benchmark in an emulated multi-site architecture and crawling from a local repository.
Then, we report several experimental results where we deploy \crawler at multiple locations in Germany and access actual web sites.

\subsection{In-vitro validation}
\labsection{evaluation:invitro}

This set of experiments evaluates the performance of \crawler in a controlled environment.
To that end, we first assess the scalability of our approach in the case of a single site.
Then, we present several experimental results that were conducted in an emulated multi-site architecture.

Our experiments take place on a cluster of virtualized 8-core Xeon 2.5~Ghz machines with 8GB of memory.
Nodes are running Ubuntu 14.04 64bits, and are connected with a virtualized 1~Gbps switched network.
Network performance, as measured by \emph{ping} and \emph{netperf}, is 0.3ms for the round-trip delay and a bandwidth of 117~MB/s.
We set-up Yarn to use 4~GB of the total node memory; the other half being used by \ispn.
A mapper (or a reducer) can use up to 1~GB of memory.
Each node is equipped with a virtual hard-drive whose read/write (uncached) performance, as measured with \texttt{hdparm} and \texttt{dd}, is 97/91~MB/s.

Our in-vitro evaluation uses the ClueWeb12 B13 dataset~\cite{clueweb12}.
This dataset contains around 52 millions documents, which were gathered by the Lemur project~\cite{lemurproject} in February 2012.
In total, this dataset weights 1.95~TB and we serve it via a dedicated Python server.
During all our experiments, we start by injecting around 1 million URLs in the crawl database, 
then we execute several crawling rounds with a width of 50,000 pages.

\subsubsection{Single site performance}
\labsection{evaluation:invitre:site}

\reffigure{invitro:single} present the average length of each crawl phase at a site, when we deploy \crawler on 1 to 4 nodes.
As indicated in \refsection{implementation:merge}, \crawler merges the fetch and parse phases.
Consequently, we only report the average time a fetch phase takes to execute.

In \reffigure{invitro:single}, we can observe that the total time of a round decreases from 1,337 to 312 seconds when \crawler scales out from 1 to 4 nodes.
In term of pages per second, this translates into a 3.79 multiplicative factor, 4 nodes being able to process around 160 pages/s.
In this figure, we also observe that the time required to execute the fetch phase with 3 and 4 nodes is close.
This comes from the fact that our Python server hosting the ClueWeb12 dataset saturates at around 380 pages/s.
Besides, with a crawl width of 50,000 pages, the time to execute both the generate phases does not change much, even with 4 nodes.
This cost corresponds essentially to the time the map-reduce framework takes to propagate the jobs across workers and start the Java VMs.

\subsubsection{Emulating multiple sites}
\labsection{evaluation:invitre:multi}

Next, we evaluate how \crawler behaves in a multi-site environment.
To that end, we emulate the long-distance routing between geo-distributed locations with the help of the Linux traffic shaping tools.
Our experiment makes use of 3 sites, each site containing 3 \crawler nodes.
We fix to 6 the number of reducers per site. 
Besides, to avoid saturating the Python server hosting the ClueWeb12 benchmark, we also set the number of fetcher threads per reducer to 20.

In \reffigure{invitro:multi}, we vary the ping distance between any two locations, from 20 to 80 ms.
When the ping distance equals 20ms, this corresponds to a deployment at the scale of an European country.
In the case of 80 ms, we emulate a cross-continent deployment.
\reffigure{invitro:multi} reports on the left y axis (in black) the average throughput of \crawler in number of pages per second, aggregated over all sites.
The right y axis (in gray) reports the average length of the update phase during a crawling round.

As expected, we observe in \reffigure{invitro:multi} that the more distant the sites are, the slower \crawler is.
Peeking from 334 pages/s when the sites are geo-graphically close, performance deteriorates to 278 pages/s when the ping distance equals 80ms.
Moreover, we notice in \reffigure{invitro:multi} that \crawler performance is linearly correlated with the time it takes to execute an update phase.
Such a correlation is expected from the design of \crawler, as only the update phase necessitates some collaboration between the sites.

\begin{figure}[!t]
  \centering
  \subfigure[Single site scalability]{\includegraphics[width=0.28\textwidth]{plots/invitro/single/single.pdf}\labfigure{invitro:single}}
  \subfigure[Impact of geo-distribution]{\hspace{1.1em}\includegraphics[width=0.28\textwidth]{plots/invitro/multi/multi.pdf}\labfigure{invitro:multi}}
  \caption{
    \labfigure{invitro}
    Performances in-vitro 
  }
\end{figure}

\subsection{\crawler in the wild}
\labsection{evaluation:invivo}

%cloud0=Hamburg, cloud1= M\¨unster
\begin{figure}[!t]
  \centering
  \subfigure[Locations of the clouds]{\includegraphics[width=0.29\textwidth]{figures/germany.pdf}\labfigure{invivo:location}}
  \subfigure[Exchange rate at Hamburg]{\includegraphics[width=0.29\textwidth]{script/filter/cachehit-cloud0.pdf}\labfigure{invivo:exchange}}
  \subfigure[Network traffic]{\includegraphics[width=0.29\textwidth]{script/traffic/networktraffic.pdf}\labfigure{invivo:traffic}}
  \subfigure[Scalability]{\includegraphics[width=0.29\textwidth]{script/time/phases-time.pdf}\labfigure{invivo:scalability}}
  \caption{
    \labfigure{invivo}
    Evaluation in a geo-distributed setting
  }
\end{figure}

To further assess the design of \crawler, we measure its performance in various real-world scenarios.
We use several clouds provided by the Cloud \& Heat company~\cite{cloudandheat}, and deploy \crawler at different locations in Germany.
A cloud operates 3 instances (VMs) of medium size (4~GB RAM, 4 virtual CPU and 120~GB disk), connected via a gigabit ethernet and running Ubuntu Linux 14.04 64 bits.
\reffigure{invivo:location} indicates the clouds' locations.

In all the experiments that follow, we start our crawl from a seed list of 30 US universities.
We fix to 6 the number of reducers per site and use the default Nutch value of 10 fetcher threads per reducer.
Our evaluation consists then in a sequence of 50 crawling rounds.
Below, we first comment on the benefits of our caching mechanism.
Then, we compare \crawler to an out-of-the-box Nutch deployment.
We close our evaluation with an assessment of the scalability of our design.

\subsubsection{URL Exchange}
\labsection{evaluation:invivo:exchange}

In \reffigure{invivo:exchange}, we evaluate the URLs exchange rate at Hamburg, when \crawler spans both this location and M\"unster.
We observe that on average the Hamburg site finds 2,981 duplicate URLs per round, with an average hit rate of 92\%.
After 50 crawling rounds, this translates into a memory usage of around 3~MB for the URLs cache.
This performance, inline with the simulation results of \citet{broder03efficienturlcaching}, shows the benefits of the caching mechanism.

\subsubsection{Comparison with Nutch}
\labsection{evaluation:invivo:comparison}

Our second experiment aims at assessing the advantages of our design in comparison to a naive deployment of Nutch with a Cassandra\footnote{http://cassandra.apache.org} backend over multiple geographical locations.
To that end, we deploy \crawler and Nutch at both Hamburg and M\"unster, and measure the traffic consumption between sites.
\reffigure{invivo:traffic} details our results in a semi-log scale.
Notice that in this experiment we fix the crawl width in such a way that both systems harvest in total a similar amount of pages (respectively 184K and 190K for \crawler and Nutch).

We observe in \reffigure{invivo:traffic} that \crawler is around 1.75 times faster than the Nutch deployment.
Such a gap comes from the fact that Nutch executes the map-reduce steps of the crawling phase over the two locations, 
which implies a large penalty in term of processing time.
In addition, this figure tells us that our approach reduces the inter-site traffic by 93.6\%.
This last point is of importance as WAN bandwidth is notoriously expensive.
For instance, the very same Nutch deployment across two Amazon clouds would require around 3000 GB/month and costs 30 USD/month.\footnote{At the time of this writing, and according to \url{http://calculator.s3.amazonaws.com/index.html}.}
On the other hand, \crawler requires only around 192 GB/month (corresponding to 1.92 USD/month).

\subsubsection{Scalability}
\labsection{evaluation:invivo:scalability}

Our last experiment consists in scaling-up \crawler over multiple locations.
\reffigure{invivo:scalability} reports the average time of each phase when we deploy our system on two and three locations.
We also show a comparison with a naive Nutch deployment at two sites.
In all experiments, we fix the global crawl width to a constant factor of 400.

In this figure, we first observe that the key issue with the naive Nutch deployment comes from the update phase.
Indeed, this phase is an order of magnitude slower than with a deployment of \crawler over the two same locations.
We can explain this difference by the fact that the map-reduce steps are geo-distributed and that the update phase is the most demanding, both in terms of storage and processing power.
\reffigure{invivo:scalability} also tells us that the total time to execute a crawling round improves when we scale \crawler from two to three sites. 
This improvement comes from the fact that the global crawl width is constant in both cases.
In this setting, \crawler displays a scale-up factor of 1.42 from two to three sites.

