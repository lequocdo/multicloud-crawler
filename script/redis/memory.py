import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys

fontsize = 9

pylab.rc("font", family= 'sans-serif') #"serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 4.2
height = width/1.02

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

linestyle=['-.', '.-']
method2color={'cloud0': '#111111', 'cloud1' : '#000000'}
plot_order = ['cloud0', 'cloud1']
def plot_single_figure(xs, ys, filename, xlabel='', ylabel='',
                       labels=None,
                       customize=None) :

    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)
    
    for (i, y) in enumerate(ys) :
        print i
        #if i == 0:
        method = plot_order[i]
        if i == 0:
            plt.plot(xs[i], y, '-', color=method2color[method], label=labels[i], linewidth=1)

        if i == 1:
            plt.plot(xs[i], y, '-.', color=method2color[method], label=labels[i], linewidth=1)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    ax1.set_xticks([10, 20, 30, 40, 50])

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    plt.legend(loc=2, numpoints=1 )
    leg = plt.gca().get_legend()
    if leg :
        leg.draw_frame(False)
        ltext = leg.get_texts()
        plt.setp(ltext, fontsize=fontsize)
        plt.setp(leg.get_frame(), linewidth=0.2)

    plt.savefig(filename)

##############INPUT DATA ###################
#xs = [range(10), range(10)]
#ys = [range(10), range(10,20)]
x1 = range(0, 51)
y1 = []
x2 = range(0, 51)
y2 = []

f1 = open("ram-cloud0.txt", "rb")
for line in f1.readlines():
    y1.append(float(line))

f2 = open("ram-cloud1.txt", "rb")
for line in f2.readlines():
    y2.append(float(line))

xs = [x1,x2]
ys = [y1,y2]
#print xs
#print ys
f1.close()
f2.close()
plot_single_figure(xs, ys,
                   'redis-memory',
                   ylabel='Accumulated Memory Consumption [MB]',
                   xlabel='Crawling Rounds',
                   labels=["Cloud0", "Cloud1"])

