import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys

fontsize = 36
width = 14
height = 12

pylab.rc("font", family='sans-serif', size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

linestyle=['-.', '.-']
method2color={'Modified Nutch': '#111111', 'Native Nutch' : '#bbbbbb'}
plot_order = ['Modified Nutch', 'Native Nutch']
def plot_single_figure(xs, ys, filename, xlabel='', ylabel='',
                       labels=None,
                       customize=None) :

    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)
    
    for (i, y) in enumerate(ys) :
        method = plot_order[i]
        plt.semilogy(xs[i] , y, color=method2color[method], label=labels[i], linewidth=1, linestyle="-")

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.xaxis.labelpad = 20
    ax1.yaxis.set_ticks_position('left')
    ax1.yaxis.labelpad = 20
    ax1.set_xticks([20000, 40000, 60000])
    # ax1.set_yticks([0,20,40,60,80,100,120])
    ax1.set_ylim(0, 10000000)
    # ax1.set_xlim(0, 33)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    plt.legend(loc=1, numpoints=1 )
    leg = plt.gca().get_legend()
    if leg :
        leg.draw_frame(False)
        ltext = leg.get_texts()
        plt.setp(ltext, fontsize=fontsize)
        plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig(filename)

##############INPUT DATA ###################
#xs = [range(10), range(10)]
#ys = [range(10), range(10,20)]


#x1 = range(0, 47109)
x1 = range(0, 44179)
y1 = []
x2 = range(0, 72999)
y2 = []

f1 = open("unicrawl-traffic.txt", "rb")
for line in f1.readlines():
    y1.append(float(line)*8/1000)

f2 = open("native-nutch-traffic.txt", "rb")
for line in f2.readlines():
    y2.append(int(line)*8/1000)

xs = [x1,x2]
ys = [y1,y2]
#print xs
#print ys
f1.close()
f2.close()
plot_single_figure(xs, ys,
                   'networktraffic',
                   ylabel='Throughput [Kbps]',
                   xlabel='Crawling time (sec)',
                   labels=["unicrawl", "nutch"])

