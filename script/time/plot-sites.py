#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
#matplotlib.use('PDF')
import pylab 

##################################################
filename = "phases-time.pdf"

fontsize = 36
width = 14
height = 12

pylab.rc("font", family='sans-serif', size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5
fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')
ax1.yaxis.labelpad = 20

matplotlib.use('PDF')
#################################
def sumzip(*items):
    return [sum(values) for values in zip(*items)]

#################################
generate = (103.436, 55, 43)
fetch = (766.143, 509, 290)
updatedb = (1583.859, 144, 95)
index = np.arange(3)    # the x locations for the groups
width = 0.3       # the width of the bars: can also be len(x) sequence

#p1 = plt.bar(index, insert, width, color='#000000', linewidth=0.2)
p2 = plt.bar(index, generate, width , color='#444444', linewidth=0.2)
p3 = plt.bar(index, fetch, width, color='#888888', bottom=sumzip(generate), linewidth=0.2)
#p4 = plt.bar(index, parse, width, color='#BBBBBB', bottom=sumzip(generate, fetch), linewidth=0.2)
p5 = plt.bar(index, updatedb, width, color='#EEEEEE', bottom=sumzip(generate, fetch), linewidth=0.2)


#######################################################################
plt.ylabel("Time consumption [seconds]")
plt.xticks(index+width/2 , ("Nutch", "2 sites", "3 sites") )
ax1.set_yticks([100, 500, 1000, 1500, 2000, 2500])
plt.legend((p2[0], p3[0], p5[0]), ('generate', 'fetch', 'update'), loc=1)


for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

plt.legend(loc='upper left', numpoints=1 )
leg = plt.gca().get_legend()
if leg :
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.2)

plt.savefig(filename)

