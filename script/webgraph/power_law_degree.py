#DoLeQuoc@2015
from __future__ import division
import matplotlib.pyplot as plt
import networkx as nx
import sys
from pylab import *

import power_law as pl



#########################################################################
G=nx.read_dot(sys.argv[1]) # Read Graph

degree_sequence=sorted(nx.degree(G).values(),reverse=True) 
#degree sequence
#sdmax=max(degree_sequence)

print "\n number of nodes:",G.number_of_nodes()
print "\n number of Links:",G.size()
print "\n Degree histogram",nx.degree_histogram(G)
#########################################################################
#Out degree histogram
degseq=G.out_degree().values()
dmax=max(degseq)+1
out_hist = [ 0 for d in xrange(dmax) ]
for d in degseq:
    out_hist[d] += 1
figure(0)
plt.loglog(out_hist,'r',marker='o',ls="-")
plt.title("Out_degree Histogram rank plot")
plt.ylabel("frequency")
plt.xlabel("degree")
plt.savefig("image/out_degree.png")
#plt_out.show()

##########################################################################
#In degree histogram
degseq=G.in_degree().values()
dmax=max(degseq)+1
in_hist = [ 0 for d in xrange(dmax) ]
for d in degseq:
    in_hist[d] += 1
figure(1)
plt.loglog(in_hist,'r',marker='o',ls="-")
plt.title("In_degree histogram plot")
plt.ylabel("frequency")
plt.xlabel("degree")
plt.savefig("image/in_degree.png") 

###########################################################################
#degree
figure(2)

power_law = pl.power_law_degree_exponent(G) 
print "Degree Distribution Power Law Exponent", power_law
plt.loglog(nx.degree_histogram(G),'r',marker='o',ls="-")
#plt.loglog(hist_order,y,'r',marker='o',ls="-")
#plt.loglog(x,avr,'r',marker='o',ls="-")
#plt.title("Degree distribution plot")
plt.ylabel("Distribution")
plt.xlabel("Degree")

#plt.savefig(sys.argv[2])
plt.savefig("image/degree.pdf")
#plt.show()

###########################################################################
import powerlaw
import numpy as np
#data = nx.degree(G)
sequence_degree=sorted(G.degree().values(),reverse=True)
data = np.asarray(sequence_degree)

print "#######################################"
#print data

#import plfit
#myplfit=plfit.plfit(data,usefortran=True)
#print myplfit
#######testt############3
#data=plfit.plexp_inv(rand(1000),1,2.5)


fit = powerlaw.Fit(data)
alpha = fit.power_law.alpha
sigma = fit.power_law.sigma
print "New alpha ", alpha
print "New sigma", sigma
#powerlaw.plot_pdf(data, color='b')
figPDF = powerlaw.plot_pdf(data, color='b')
powerlaw.plot_pdf(data, linear_bins=True, color='r', ax=figPDF)
####
#figPDF.set_ylabel(r"$p(X)$")
figPDF.set_ylabel(r"Distribution")
figPDF.set_xlabel(r"Degree Frequency")
plt.savefig('law-distribution-fit.pdf', bbox_inches='tight')

