import networkx as nx
import numpy as np
import scipy.special as special
import scipy.optimize as opt

def complement_cumulative_distribution(dist):
    """Return the complement cumulative distribution(CCD) of a list dist

    Returns the CCD given by
            P(X>x) = 1 - P(X<x)
    where P(X<x) is the cumulative distribution function given by
            P(X < x) = sum_{xi<x} p(xi)
    where p(xi) is the probaility density function calculated as
            p(xi) = xi/(sum_i xi)

    Parameters
    ----------
    dist : list of Numeric
           list of values representing the frequency of a value at
           the index occuring

    Returns
    -------
    ccdh : List of floats
           A list of the same length, as the cumulative complementary
           distribution function.
    """

    return [1-(float(csdist)/sum(dist)) for csdist in np.cumsum(dist)]

def complement_cumulative_degree_distribution(G):
    """ Return the Complement Cumulative Degree Distribution as defined
        in complement_cumulative_distribution

    Parameters
    ----------
    G : a Graph
        Graph on which to determine the CCDD

    Returns
    -------
    ccdh : list of numeric
           The complement cumulative degree distribution
    """    
    dh = nx.degree_histogram(G)
    return complement_cumulative_distribution(dh)

def discrete_power_law_neg_log_likelihood(a,xmin,xs):
    return float(len(xs)*np.log(special.zeta(a,xmin)) + a*sum([np.log(x) for x in xs]))

"""Not currently used
def continuous_power_law_max_likelihood(xmin,xs):
    return 1.0 + len(xs)*(1/sum([np.log(x/xmin) for x in xs]))
"""

def power_law_degree_exponent(G):
    """ Returns the exponent of the power law of the degree distribution
    or at least attempts to find one.

    If the degree distribution follows the power law

        p(x) \prop x^-a

    where 2 < a < 3 generally.[1]

        "... one can estimate a by direct numeric maximization of the
        likelihood function itself, or equivalently of its logarithm
        (which is usually simpler):

        L(a) = -n ln(zeta(a,xmin)) - a sum(ln(xi))"[1]

    In the discrete case, which degrees are.

    We actually minimize the the -log likelihood as this is the same

    While this function will actually return the exponent for the graph
    this doesn't necessarily mean the graph has a power law degree
    distribution. Other statistical tests should be run to build evidence
    for that hypothesis. Moreover, this assumes a power law for all values
    of degree > 0. It is possible that the distribution follows a power-law
    only in the tail for some value greater than dmin. Other tests in the
    future will be needed.

    Parameters
    ----------
    G : Graph to compute the expoenent for

    Returns
    ----------
    a : float
        The estimation of the scaling parameter

    [1] A. Clauset, C.R. Shalizi, and M.E.J. Newman,
        "Power-law distributions in empirical data"
        SIAM Review 51(4), 661-703 (2009). (arXiv:0706.1062)

    A little explaining about this function opt.fmin. It takes a
    function that represents the function we are trying to minimize,
    in this case the negative log likelihood of the discrete power
    law function. It takes an initial guess 2.5, which is where most
    power laws are anyway and args which tells it what arguments to
    pass to the function we are optimizing besides a, and finally we
    turn the display off with disp=0
    """
    d = G.degree().values()
    dmin = np.min(d)
    a = opt.fmin(discrete_power_law_neg_log_likelihood,2.5,args=(dmin,d),disp=0)
    return float(a)

