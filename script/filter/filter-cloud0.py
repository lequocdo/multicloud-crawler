import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys

fontsize = 3
width = 14
height = 12

pylab.rc("font", family='sans-serif', size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

linestyle=['-.', '.-']
method2color={'sent': '#111111', 'duplicate' : '#bbbbbb'}
plot_order = ['sent', 'duplicate']
def plot_single_figure(xs, ys, filename, xlabel='', ylabel='',
                       labels=None,
                       customize=None) :

    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)
    
    for (i, y) in enumerate(ys) :
        method = plot_order[i]

        if i == 0:
            plt.bar(xs[i], y, 0.3, color=method2color[method], label=labels[i], linewidth=0)
        if i == 1:
            x = [a + 0.3 for a in xs[i]]
            plt.bar(x, y, 0.4, color=method2color[method], label=labels[i], linewidth=0) #width=0.35 print 2 columns

    ax1.xaxis.set_ticks_position('bottom')
    ax1.xaxis.labelpad = 20
    ax1.yaxis.set_ticks_position('left')
    ax1.yaxis.labelpad = 20
    ax1.set_xticks([0, 10, 20, 30, 40, 50])

    plt.xlabel(xlabel,fontsize=fontsize)
    plt.ylabel(ylabel)

    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    plt.legend(loc=2, numpoints=1)
    leg = plt.gca().get_legend()
    if leg :
        leg.draw_frame(False)
        ltext = leg.get_texts()
        plt.setp(ltext, fontsize=fontsize)
        plt.setp(leg.get_frame(), linewidth=0.2)

    plt.savefig(filename)

##############INPUT DATA ###################
#xs = [range(10), range(10)]
#ys = [range(10), range(10,20)]
x1 = range(0, 50)
y1 = []
x2 = range(0, 50)
y2 = []

f1 = open("sent_urls_cloud0.txt", "rb")
for line in f1.readlines():
    y1.append(float(line))

f2 = open("cache0.txt", "rb")
for line in f2.readlines():
    value = line.split("|")
    y2.append(float(value[1]))

xs = [x1,x2]
ys = [y1,y2]
#print xs
#print ys
f1.close()
f2.close()

plot_single_figure(xs, 
                   ys,
                   'cachehit-cloud0',
                   ylabel='\# URLs',
                   xlabel='Crawling Rounds',
                   labels=["sent", "cache hit"])

