% -*- root: crawler-paper.tex -*-
\section{Related work}
\labsection{relatedwork}

% web crawling at a glance
While the amount of information available on the web is certainly finite, the number of pages on which that information resides is too large to be easily managed.
At core, this comes from the fact that the web is a huge, ever growing, and dynamic media.
As identified by \citet{webcrawlingsurvey}, this poses several key challenges that every crawler (sometimes referred to as a \emph{web spider}) needs to solve:
\begin{inparaenum}[(i)]
\item since the amount of information to parse is huge, a crawler must scale;
\item a crawler should select which information to download first, and which information to refresh over time;
\item a crawler should not be a burden for the web sites that host the content; and 
\item adversaries, e.g., spider traps, need to be avoided with care.
\end{inparaenum}

% web crawler designs
There exists a large number of web crawler designs and implementations.
Mercator~\cite{heydon99mercator} is an extensible web crawler written in Java.
The authors give a blueprint web crawler design and several experimental results where they cover 891 million pages in a period of 17 days.
Polybot~\cite{shkapenyuk02polybot} is a distributed system, consisting of a crawl manager, multiple downloading processes, and a DNS resolver.
IBM WebFountain~\cite{edwards01ibmwebfountain} is an industrial-strength design that relies on a central controller to operate several multi-threaded crawling agents.
Ubicrawl~\cite{boldi04ubicrawler} uses consistent hashing to partition URLs across crawling agents, leading to graceful performance degradation in the event of failures.
It was shown to be able to download about 10 million pages per day using five crawling machines at a single site.

The IRLbot system~\cite{lee09irlbot} is a single-process web crawler able to scale to large web data collections with small performance degradation.
\citet{lee09irlbot} describe a crawl that ran on a quad-CPU machine over a two months period, and downloaded nearly 6.4 billion web pages.
The Internet Archive uses the Heritrix crawler~\cite{heritrix} whose design is similar to Mercator.
Recently, the Lemur project employed Heritrix to gather the ClueWeb12 data set~\cite{clueweb12} which we use for our in-vitro experiments.
Nutch is a popular open-source web crawler that relies on the map-reduce paradigm~\cite{dean08mapreduce}.
It was notably used by \citet{roberto2014} to analyze online communities (e.g., news, blogs, or Google groups).
We also used the Nutch code base to implement \crawler{}.

All of the previous architectures we described operate several crawling agents.
\citet{cho02parcrawlers} are among the first to study the principles of distributed crawler design.
They advocate the use of a set of independent agents and propose several classifying parameters.
The assignment of URLs to agents can be either static or dynamic.
Agents might operate either in firewall, cross-over, or exchange mode when communicating each other.
Our design is in line with the conclusion of this work: 
we favor the exchange of newly discovered URLs and their static partitioning among agents.

After fetching a page, the crawler needs to check whether discovered URLs were previously crawled or not.
As a consequence, caching is a cornerstone mechanism of every web spider.
\citet{broder03efficienturlcaching} study how to efficiently cache URLs for the purpose of crawling the web. 
They ran several simulations over a trace containing 26 billions of URLs.
Their main conclusion is that a cache of roughly 50,000 entries can achieve a hit rate of almost 80\% (even when relying on a random eviction policy).

% Selecting information
All the pages on the web do not have the same importance.
Due to the scale of the Internet, important pages should be fetched first and refreshed more often.
Page importance is thus a core property that guides the discovery and refreshing of pages \cite{cho97@urlordering}.
Ranking algorithms determine what is the most significant information at the \emph{frontier} of the crawl, i.e., the set of pages whose existence is known but that are not fetched yet.
A large literature exists on ranking algorithms starting from the seminal work of Google founders on PageRank \cite{brin98pagerank}.
Following the Nutch architecture, \crawler may adapt various ranking strategies at each site. 
During our experiments, we make use of the Opic algorithm of \citet{abiteboul03opic}.
This algorithm requires a single pass over existing fetched data per crawling round, 
and is thus arguably more scalable than a full PageRank computation \cite{baeza05crawlingcountry,lee09irlbot}.

\citet{baeza04crawlingfive} study the depth at which a crawl should stop.
They introduce three stochastic models of web surfing, fit their models with information extracted from web server logs, and measure the amount of information a surfer retrieves at each level. 
Their conclusions are that most surfers stopped before depth 5 due to the fact that 80\% of the best pages are in these first levels.

% Geo-scale
Several researchers recently studied the complex challenges posed by geographically distributed web retrieval.
\citet{baeza2009feasibility} focus on the feasibility of geo-distributed search engines.
This question is also addressed in a series of paper by \citet{cambazoglu2008feasibility, cambazoglu2010query}.
In particular, these studies assess analytically the performance and interest of a web search architectures distributed over multiple sites. 
They also show that this question is of practical importance.
We can quote from their text: \emph{``it is critical to have a system design that can cope with the growth of the web, and that is not constrained by the physical limitations of a single data center''}.
\citet{exposto05geographicalpartition} show that, with an appropriate multi-criteria partitioning, it is very beneficial to split the crawling process across multiple geo-distributed agents.
\citet{baeza05crawlingcountry} compare analytically several crawling strategies to crawl geo-distributed web sites.
They show that without historical information, strategies such as largest web site first and Opic perform well (i.e., they gather the most important pages first).
\citet{baeza07challenges} identify several open problems regarding geo-distributed web retrieval:
\begin{inparaenum}
\item[(crawling)] how to properly prioritize the crawling frontier and exchange URLs between distributed agents;
\item[(indexing)] how to partition the index and balance the load across sites; and
\item[(querying)] routing and data replication matters as the cross-site network bandwidth is a scarce resource.
\end{inparaenum}
We explain in the remainder of this paper how \crawler answers those challenges, both theoretically with its design, and practically with our evaluation.
