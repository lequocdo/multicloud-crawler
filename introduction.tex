% -*- root: crawler-paper.tex -*-
\section{Introduction}
\labsection{introduction}

% The case for raw online data gathering and processing 
Over the last thirty years, publicly available digital data has grown exponentially, and it will continue to do so.
Volumes are projected to reach around 40 zettabytes by 2020, or equivalently a few gigabytes per person \cite{idc}.%
%
\footnote{
  A zettabyte corresponds to one thousand exabytes, that is $10^{21}$ bytes.
}
This data contains very rich and valuable information, and its mining and exploitation will drive growth in all sectors.
Consequently, many data-oriented companies have recently emerged and more are expected to come to the market in the coming years.
Interest for extracting intelligence from data goes beyond traditional Internet applications, such as search engines and recommendation systems, 
to areas like business planning, marketing analysis, etc.

% What is a crawler 
Most publicly available digital data on the web is in an unstructured, or loosely structured, format.
Thus, some processing is necessary in order to extract meaningful information.
The very first step of this processing, that is constructing the data collection, is usually achieved using a web crawler.
A web crawler accesses remote servers on the Internet in order to fetch, process and store web pages.
The crawler starts from some initial seed URLs, store the corresponding web pages for later processing, and then extracts links to other web pages.
Those links are themselves subsequently crawled.
This process can be repeated until a given depth, and pages are periodically re-fetched to discover new pages and to detect updated content.

% Limitations of the central crawler design
Due to the size of the web, it is mandatory to make the crawling process parallel~\cite{cho02parcrawlers} on a large number of machines to achieve a reasonable collection time.
This requirement implies provisioning large computing infrastructures.
Existing commercial crawlers, such as Google or Bing rely on big data centers.
However, this approach imposes heavy requirements, notably on the cost of the network infrastructure.
Furthermore, the high upfront investment necessary to set up appropriate data centers can only be made by few large Internet companies, leaving smaller ones out of the market.

% Public crawl data
Although public crawl repositories exist, such as Common Crawl~\cite{commoncrawl}, using them for processing requires externalizing computation and data hosting to a commercial cloud provider.
For a data-driven company, this also poses the problem of data availability in the mid-long term.
Moreover, the lack of selectivity in the public crawling process may require post-processing large amounts of data, whereas only a small subset would be necessary.

% The case for geo-distributed crawling
A solution to the above problems is to distribute the crawling effort over several geographically distributed locations.
The use of multiple sites can reduce both capital and operating expenses, for instance by allowing several small companies to mutualize their crawling infrastructures.
In addition, such an approach leverages data locality as sites can crawl web servers that are geographically nearby.
Finally, multiple sites can act as replicas to be resilient to local disasters;
collected data is then safe even if a whole site disappears.
% \er{I disagree somehow with this argument: this is something that comes from the storage layer and not from the crawler.}
On the other hand, geo-distributed crawling requires synchronization between the crawlers at the different sites, which imply communicating over a wide area network.
A careful system design for a distributed crawler aims at reducing such communication costs, as the costs and delays are higher than for intra-site communication.

% Our contribution
In this paper, we present \crawler, an efficient geo-distributed crawler that aims at minimizing inter-site communication costs. 
\crawler orchestrates multiple geographically distributed sites.
Each site uses an independent crawler and relies on well-established techniques for fetching and parsing the content of the web.
\crawler partitions the crawled domain space across the sites and federates their storage and computing resources.
Our design is both practical and scalable.
We assess this claim with a detailed evaluation of \crawler in a controlled environment using the ClueWeb’12 dataset, 
as well as in geo-distributed setting using 3 distinct sites located in Germany.

\newpage
\textbf{Outline}
The rest of the paper is organized as follows.
We survey related work in \refsection{relatedwork}.
\refsection{design} introduces the crawler architecture, refining it from existing well-founded central designs.
Details about the implementation internals are given in \refsection{implementation}.
\refsection{evaluation} presents the experimental results, both in-vitro, and in-vivo over multiple geographical locations in Germany.
We discuss our results and future work in \refsection{discussion}.
We conclude the paper in \refsection{conclusion}.
