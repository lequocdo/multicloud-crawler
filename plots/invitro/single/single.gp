set term postscript color eps enhanced 36
set output "single.eps"

set style histogram rowstacked
set style data histogram

set style fill solid 1.00 noborder
set boxwidth 0.5 absolute

set ytics 200
set yrange [0:1400]
set ylabel 'spent time (sec)'
set xlabel 'nodes'

set style line 1 lt 1 lw 3 pt 2 lc rgb "#111111"
set style line 3 lt 1 lw 3 pt 2 lc rgb "#444444"
set style line 5 lt 1 lw 3 pt 2 lc rgb "#bbbbbb"

plot 'single.dat' using 2:xtic(1) ls 1 t 'generate', '' using 3 ls 3 t 'fetch', '' using 4 ls 5 t 'update'

!epstopdf single.eps
!rm single.eps
quit
