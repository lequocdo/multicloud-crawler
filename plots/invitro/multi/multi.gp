set term postscript color eps enhanced 36
set output "multi.eps"

set style data histogram

set style fill solid 1.00 noborder
set boxwidth 1 absolute

set ytics 20
set y2tics 20
set ylabel 'throughput (pages/s)'
set yrange [220:360]
set y2label "update phase (sec)"
set y2range [100:240]
set xlabel 'inter-site delay (ms)'

set style line 1 lt 1 lw 3 pt 2 lc rgb "#111111"
set style line 2 lt 1 lw 3 pt 2 lc rgb "#bbbbbb"

plot 'multi.dat' using 2:xtic(1) ls 1 t '', '' using 3 ls 2 t '' axes x1y2

!epstopdf multi.eps
!rm multi.eps
quit
