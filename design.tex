% -*- root: crawler-paper.tex -*-
\section{Distributed Crawler Architecture}
\labsection{design}

The general algorithmic sequence used by a web crawler is quite simple.
It can be seen as the continuous execution of the following four phases.
In the \emph{generate} phase, the crawler extends the crawl frontier by determining the next set of URLs that it needs to fetch.
These URLs are downloaded from web hosts in the \emph{fetch} phase.
The crawler analyzes the content retrieved during the \emph{parse} phase and refreshes appropriately the crawl database in the \emph{update} phase.

However, the size of the web and its rate of change (estimated at 7\% per week \cite{brewington00webdynamicity}) introduce some serious system design issues.
This section presents how the design of \crawler addresses those problems, describing first the internals of a site, then cross-site operations.

\subsection{Single site Design}
\labsection{design:single}

At each site, we build \crawler upon the well-established architecture of Apache Nutch~\cite{nutch}, more specifically its version 2.x.
We contribute to Nutch the necessary improvements and novel features that we shall cover next.

\reffigure{site} depicts an overview of the architecture.
\crawler makes use of the map-reduce paradigm and persists the content of the crawl in a distributed key-value store.
This design choice, inherited from Nutch, ensures that the system is able to process large amount of data.
In what follows, we recall how map-reduce works, detail the storage internals, then explain how we combine the two to implement the various phases of a crawling round.

\subsubsection{Map-reduce}
\labsection{design:single:mr}

Map-reduce is a paradigm for processing large data sets in parallel on a cluster of workers.
Schematically, map-reduce executes three steps, each step operating over a set of key-value pairs.
In the \emph{map} step, each worker applies a $\mathit{map}()$ function to its local \emph{split} of the pairs, and \emph{spills} the output to some temporary storage. 
During the \emph{shuffle} step, workers distributes pairs stored in temporary storage among them, ensuring that all data corresponding to the same key is located on a single worker.
Then, workers in the reduce step process output data grouped per key, applying to it a $\mathit{reduce}()$ function.

\subsubsection{Site storage}
\labsection{design:single:storage}

Every web spider needs to persist a large amount of information over time:
the fetched pages themselves, but also other data structures that maintain the state of the crawling process.
To that end, a crawler uses a \emph{crawl database}.
In \crawler, the crawl database of a site is implemented as a single distributed map structure.
This map contains for each page its URL, content, and outlinks (i.e., the URLs of pages linked from it).
In addition, it also contains several additional metadata fields used during the crawl.
In particular, the crawl database stores the page status (generated, fetched, moved, etc.) and its \emph{score} attributed by the ranking algorithm.
Notice that the score of two pages are always comparable, and that this order defines the \emph{ranking} of a page.

To implement the crawl database, \crawler makes use of \ispn \cite{marchioni2012infinispan}, a distributed key-value store that supports the following features:
\begin{inparaenum}
\item[\emph{(Routing)}]
  Nodes are organized in a ring.
 \ispn uses a one-hop routing design, i.e., every node knows all the other nodes.
\item[\emph{(Elasticity)}]
  \ispn is elastic, meaning that storage nodes can be added or removed on the fly.
  Upon joining, a node chooses a random identifier along the ring and fetches the ring structure from some other \ispn node.
  It then informs its neighbors that it is joining.
\item[\emph{(Storage)}]
  \ispn uses consistent hashing~\cite{consistenthashing} to assign blocks to nodes with a replication factor $\rho$: 
  a data block with a key $l$ is stored at the $\rho$ nodes whose identifiers follow $l$ on the ring.
\item[\emph{(Reliability)}]
  \ispn is built upon the JGroups communication library~\cite{jgroups}.
  This library use failure detectors to maintain a consistent view of the system.
  The repair mechanisms of consistent hashing are triggered upon a lack of response of a storage node within a timeout.
\item[\emph{(Interface)}]
  \ispn offers to the end-user a concurrent map interface that provides the classical \aput, \get, \remove and \putIfAbsent operations.
\item[(\emph{Consistency})]
  \ispn implements strongly consistent operation on the distributed map using a primary-backup replication scheme.
\item[\emph{(Querying)}]
  Every \ispn node maintains a configurable index of the data it stores. 
  This index is purely local, and based on Apache Lucene~\cite{lucene}.
  It allows to execute SQL-like queries over the indexed content of the node.
  % FIXME describe clustered queries 
\end{inparaenum}

\begin{figure}[!t]
  \centering
  \subfigure[Site architecture]{\includegraphics[clip,width=0.18\textwidth]{figures/site.pdf}}
  \hspace{1em}
  \subfigure[Crawling phases]{\includegraphics[width=0.2\textwidth]{figures/phases.pdf}}
  \caption{
    \labfigure{site}
    Overview of \crawler at a site.
  }
\end{figure} 

\subsubsection{Crawling phases}
\labsection{design:single:phases}

In \reffigure{site}, we present the different crawling phases of \crawler.
\crawler executes one map-reduce job per phase.
Schematically, a job first fetches data from \ispn with an appropriate query, 
executes some computation upon it, then stores the corresponding results during the reduce phase. 

Following the map-reduce paradigm, for each phase, we execute one instance of the input query per storage node.
In addition, we rely on pagination to further increase parallelism.
To that end, we compute tentatively the amount of results at each storage node, then split the original query according to some pagination level. 
This leverages the fact that multiple mappers can process in parallel the data available from the same local \ispn node.

We now detail each of the phases which compose a crawling round.
\begin{itemize}
\item[(\emph{Generate})]
  The goal of the generate phase is to select a set of pages to process during the round.
  To that end, the map step computes the subset of pages in the crawl database which were not previously fetched during the previous rounds.
  These pages are grouped by host in the reduce phase, and each reducer outputs the $k$ top ranked pages.
  %
  In total, $r$ reducers generates $rk$ pages, and thus $rk$ defines the crawl \emph{width} at a site.
  In \crawler, the query at each storage node retrieves only the $rk$ top ranked pages.
  This improvement, not present in the original Nutch design, ensures that at each round the complexity of the generate phase is bounded by the crawl width.
\item[(\emph{Fetch})]
  During the fetch phase, the map step first groups by host the pages that were generated in the previous phase.
  In the reduce step, each worker receives a set of hosts together with the list of web pages it needs to retrieve from those hosts.
  A reducer is multi-threaded, yet in order to maintain politeness it uses a single queue per host.
  This queue is subject to a configurable wait policy.
  Moreover for each host, the reducers follow the instructions given in \emph{/robots.txt}.
\item[(\emph{Parse})]
  Once the pages are fetched, they are analyzed during the parse phase.
  This phase consists solely of a map step.
  During this step, the mapper extracts from the fetched pages stored locally the set of outlinks it contains and add them to the crawl database.
\item[(\emph{Update})]  
 The goal of the update phase is to refresh the scores of pages that belong to the frontier in order to prioritize them.
 To that end, we use the OPIC algorithm of \citet{abiteboul03opic}.
 This scoring method is less expensive than a full PageRank computation and suitable for on-line crawling.
 Moreover, as shown by \citet{baeza05crawlingcountry}, it quickly converges toward pages of importance.
 %
 At the beginning of the update phase, the map step retrieves both the score and outlinks of each fetched page.
 Then, it splits the score evenly across outlinks, and outputs for each outlink its URL together with the shard of the score.
 In the reduce step of the update phase, each reducer sums the scores obtained by pages located at the frontier.
 Pages that are among the top $l > k$ ranked ones are then added to the crawl database.
 Depending on the crawl width, part or all of these pages will then be generated at the beginning of the next crawling round.
 %
 In Nutch, the update phase process all the pages in the crawl database.
 On the contrary, \crawler executes the map step only on the fetched pages and adds solely $rl$ new pages to the crawl database.
\end{itemize}

\subsection{Multi-site Operations}
\labsection{design:multi}

\reffigure{multi} depicts the architecture of \crawler when we deploy it across multiple geographical locations.
In a typical set-up, \crawler is composed of several sites interconnected with a wide-area network, 
and each site is equipped with a few dozen of machines communicating through a low-latency network.

\begin{figure}[t]
  \centering
  \subfigure{\includegraphics[width=0.37\textwidth]{figures/multi.pdf}}
  \caption{
    \labfigure{multi}
    Multisite architecture of \crawler.
  }
\end{figure} 

Several key ideas allow \crawler to be practical in this setting:
\begin{inparaenum}[(i)]
\item Each site is independent and crawls the web autonomously;
\item We unite all the site data stores.
  At each site, the map-reduce jobs of \crawler access transparently the federated storage which can provide dependability through geo-replication; and
\item Sites exchanges dynamically the URLs they discover over the course of the crawl.
\end{inparaenum}

In the sections that follow, we cover the multi-site operations of \crawler in detail.
Furthermore, we discuss the crawl quality in regard to the amount of communication between sites.

\subsubsection{Federating the storage}
\labsection{design:multi:storage}

One of the key design concerns of \crawler is to bring small modifications to the site code base in order be usable over multiple geographical locations.
To achieve this, we rely on \ensemble, a storage layer able to federate transparently multiple \ispn deployments.

\ensemble offers the same concurrent map interface to the map-reduce jobs of \crawler as \ispn.
An \ensemble map is built upon a set of \ispn maps and it can be either \emph{replicated} or \emph{distributed}.
If the map is replicated, all the underlying \ispn maps replicate its content.
On the other hand, if the \ensemble map is distributed, each \ispn map stores a distinct part of the content.
In this case, a partitioner defines how the content is split between the \ispn maps.

A distributed map can operate in \emph{normal} or \emph{frontier} mode.
In normal mode, \aput and \get operations access the exact locations of the content, possibly on another site than the one where the call was made.
When using the frontier mode, \aput operations behave correctly and may be remote, but \get operations and queries are local to the site that call them.
We use this later mode to implement the crawl database.
The next section covers with more details how we proceed.

\subsubsection{Collaboration between sites }
\labsection{design:multi:collaboration}

\begin{figure}[t]
  \centering
  \subfigure{\includegraphics[width=0.45\textwidth]{figures/lifetime.pdf}}
  \caption{
    \labfigure{lifetime}
    Lifetime of the update phase.
  }
\end{figure} 

Following the approach advocated by \citet{cho02parcrawlers}, \crawler exchanges newly discovered URLs over time.
This exchange occurs at the end of the update phase.
We depict the lifetime of this phase in \reffigure{lifetime}.

In detail, we implement the crawl database as a distributed \ensemble map that span all the sites.
This map operates in frontier mode with a replication factor of one.
As a consequence of this setting, 
\begin{inparaenum}[(i)]
\item in the reduce step of the update phase, the reducers write the $kn$ top ranked pages across all sites, while
\item all the keys accessed by the generate, fetch and parse phases are local to the site.
\end{inparaenum}
This allows sites to operate independently.
Moreover, it reduces communication to the bare minimum of newly discovered URLs.

We can tune the partitioner which assigns each key in the \ensemble map to an underlying \ispn site map.
In our current setting, we use two approaches to that end: consistent hashing and distance-based.
The first solution is similar to the initial proposal of \citet{boldi04ubicrawler}.
The distance-based partitioner is more involved but reduces the distance between a web server and its corresponding fetching site, thus lowering the network cost associated to it.
This partitioner relies on an \ensemble map $D$ replicated at all sites which associates a domain to its geographical coordinates.
For some page $p$ having URL $u$, when a $\aput(u,p)$ operation occurs on the \ensemble map implementing the crawl database, 
the partitioner first extracts the domain $d$ of $u$, then it executes a $\get(d)$ operation on $D$.
If the coordinates do not exist, the partitioner retrieves them using the IP address of the domain.
Once the coordinates of $d$ are present in $D$, the partitioner computes the closest geo-graphical site and returns the associated \ispn map.

Notice that in \crawler, sites operates independently and execute their update phases at different times.
As a consequence, a site that finds a new URL uses a \putIfAbsent operation when it accesses a distant site storage.
This avoid race condition in case this URL was already crawled.
Furthermore, during the update phase, each reducer 
\begin{inparaenum}[(i)]
  \item outputs at most $\frac{l}{m}$ URLs where $m$ is the number of participating sites, and
  \item stops after it has retrieved $l$ URLs local to its site.
\end{inparaenum}
The former modification avoids to overwhelm a site, whereas the latter preserves them from starvation.

\subsubsection{Crawl quality and cost}
\labsection{design:multi:quality}

The quality of the crawling operation is not only measured by means of pure web-graph exploration but also by the rounds it takes to discover the most interesting pages.
This is important because web crawling and web searching are two algorithms that are executed concurrently.
Due to the massive size of the web, state-of-the-art crawlers focus on finding the most relevant portion of the internet as quickly as possible \cite{abiteboul03opic}. 
As we pointed out previously, they use to that end a scoring algorithm that estimates the importance of each page.
The crawler prioritizes its crawling effort on the URLs with the highest scores. 

Recall that parameter $k$ determines the crawl width, and $r$ the total amount of reducers during the generate phase.
The generate phase outputs the $rk$ most ranked pages in its frontier.
This rank is attributed to pages from the OPIC algorithm computation that occurs during the update phase.
As a consequence, when \crawler operates a single site, it fetches the $rk$ most ranked pages in the frontier, 
and with good probability these pages are the most important ones (according to their PageRank~\cite{abiteboul03opic,baeza05crawlingcountry}).

In a geo-distributed setting, sites executing \crawler exchange URLs between them at the end of the update phase.
In case a site A retrieves an URL $u$ assigned to some site B, it calls the \putIfAbsent primitive (see \reffigure{lifetime}).
This primitive has no effect if $u$ is already stored at site B.
As a consequence, the score at site B might not take into account all the contributions of pages stored at site A.
Thus, \crawler may not always prioritize the most important pages in a geo-distributed setting.
On the other hand, such an approach reduces the volume of data exchanged between sites.

When \crawler spans several locations, the key cost parameter is the amount of communication exchanged between sites.
\refsection{evaluation} assesses that \crawler reduces this cost to the bare minimum.
In  \refsection{discussion}, we discuss a variation of \crawler whose crawl quality is instead optimal.
